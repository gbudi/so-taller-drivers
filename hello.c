#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/uaccess.h>

char* s;
// Device number
static dev_t hello_devno;

struct cdev hello_dev;

static struct class *hello_class;

ssize_t hello_read(struct file *filp, char __user *data, size_t size, loff_t *off) {
	printk(KERN_ALERT "%s", (const char*)s);
	copy_to_user(data, s, size);
	return 0;
}

ssize_t hello_write(struct file *filp, const char __user *data, size_t size, loff_t *off) {
	char* buffer = kmalloc(size, GFP_KERNEL);
	copy_from_user(buffer, data, size);
	if (*buffer == 'N') {
		if (s[4] != '0') s[4]--;			 
	} else if (*buffer == 'S') {
		if (s[4] != '9') s[4]++;
	} else if (*buffer == 'W') {
		if (s[2] != '0') s[2]--;
	} else if (*buffer == 'E') {
		if (s[2] != '9') s[2]++;
	}
	kfree(buffer);
	return size;
}

static struct file_operations mis_operaciones = {
	.owner = THIS_MODULE,
	.read = hello_read,
	.write = hello_write,
};

static int __init hello_init(void) {
	s = (char*) kmalloc(9*sizeof(char), GFP_KERNEL);
	//(*s) = "A 0-0: _\n";
	s[0] = 'A';
	s[1] = ' ';
	s[2] = '0';
	s[3] = '-';
	s[4] = '0';
	s[5] = ':';
	s[6] = ' ';
	s[7] = '_';
	s[8] = '\n';

	alloc_chrdev_region(&hello_devno, 0, 1, "hello");
	cdev_init(&hello_dev, &mis_operaciones);
	cdev_add(&hello_dev, hello_devno, 1);
	hello_class = class_create(THIS_MODULE, "hello");
	device_create(hello_class, NULL, hello_devno, NULL, "hello");
	return 0;
}

static void __exit hello_exit(void) {
	kfree(s);
	device_destroy(hello_class, hello_devno);
	class_destroy(hello_class);
	cdev_del(&hello_dev);
	unregister_chrdev_region(hello_devno, 1);
}



module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Juan de los Palotes");
MODULE_DESCRIPTION("Una suerte de ’Hola, mundo’");